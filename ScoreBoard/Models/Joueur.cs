﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Builder;

namespace ScoreBoard.Models
{
    public class Joueur
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z]{2,20}$", ErrorMessage = "Nom doit etre entre 2 et 20 lettres")]
        public string Nom { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z]{2,20}$", ErrorMessage = "Prenom doit etre entre 2 et 20 lettres")]
        public string Prenom { get; set; }

        [RegularExpression("^[A-Z]{2,4}$", ErrorMessage = "2 a 4 lettres majuscule")]
        public string? Equipe { get; set; }

        public string? Telephone { get; set; }

        [Required]
        [RegularExpression("^[A-Za-z0-9._%+-]+@scoreboard\\.ca$", ErrorMessage = "Veuillez suivre le modele suivant: Identifiant@scoreboard.ca")]
        public string Courriel { get; set; } 

        public List<Jeu>? Jeux { get; set; }
    }
}
