﻿using Microsoft.AspNetCore.Mvc;
using ScoreBoard.Models;

namespace ScoreBoard.Controllers
{
    public class JoueurController : Controller
    {
        private IJoueurRepository _joueurRepository;
        private IWebHostEnvironment _webHostEnvironment;

        public JoueurController(IJoueurRepository joueurRepository, IWebHostEnvironment webHostEnvironment)
        {
            _joueurRepository = joueurRepository;
            _webHostEnvironment = webHostEnvironment;
        }


        public ViewResult Index()
        {
            return View(_joueurRepository.ListeJoueurs);
        }


        [HttpGet]
        public ActionResult Creer()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Creer(Joueur joueur)
        {
            if (ModelState.IsValid)
            {
                _joueurRepository.Ajouter(joueur);

                return RedirectToAction("Index");
            }
            else
            {
                return View(joueur);
            }
        }


        public RedirectToActionResult Supprimer(int id)
        {
            int idJoueur = _joueurRepository.ListeJoueurs.FindIndex(d => d.Id == id);
            
            Joueur jou = _joueurRepository.GetJoueur(id);

            Supprimer(id);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public ViewResult Modifier(int id)
        {
            Joueur joueur = _joueurRepository.GetJoueur(id);
            ViewBag.Titre = "Modification de " + joueur.Nom;
            return View(joueur);
        }
        [HttpPost]
        public RedirectToActionResult Modifier(Joueur joueur)
        {
            _joueurRepository.Modifier(joueur);
            return RedirectToAction(nameof(Index));
        }














    }
}
